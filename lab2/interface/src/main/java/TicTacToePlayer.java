import java.rmi.Remote;
import java.rmi.RemoteException;

public interface TicTacToePlayer extends Remote {
    int onChoosePosition() throws RemoteException;

    void onOpponentMove(final Board board) throws RemoteException;

    void onLoose() throws RemoteException;

    void onWin() throws RemoteException;

    void onDraw() throws RemoteException;

    void onInvalidPositionChosen() throws RemoteException;

    void onAlreadyTakenPositionChosen() throws RemoteException;

    String nickname() throws RemoteException;

    void onError() throws RemoteException;

    void onGameInitialized(final char mark, final String opponent) throws RemoteException;
}
