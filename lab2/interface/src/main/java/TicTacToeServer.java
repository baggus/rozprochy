import exception.NicknameAlreadyTakenException;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface TicTacToeServer extends Remote {
    void join(final TicTacToePlayer player,
              final GameType gameType) throws RemoteException, NicknameAlreadyTakenException;
}
