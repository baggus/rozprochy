import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableSet;

public class Board implements Serializable {

    private static final Set<Set<Integer>> WINNING_COMBINATIONS = ImmutableSet.of(
            ImmutableSet.of(1, 2, 3),
            ImmutableSet.of(4, 5, 6),
            ImmutableSet.of(7, 8, 9),
            ImmutableSet.of(1, 4, 7),
            ImmutableSet.of(2, 5, 8),
            ImmutableSet.of(3, 6, 9),
            ImmutableSet.of(1, 5, 9),
            ImmutableSet.of(3, 5, 7)
    );

    private final TicTacToePlayer player1;
    private final TicTacToePlayer player2;
    private final Map<Integer, TicTacToePlayer> positions = new HashMap<>();

    public Board(final TicTacToePlayer player1, final TicTacToePlayer player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    public void markPlayersPosition(final TicTacToePlayer player, final int position) {
        if(isAlreadyTaken(position) || !isWithinBounds(position)) {
            throw new IllegalArgumentException("Illegal position + " + position);
        }
        this.positions.put(position, player);
    }

    public boolean isAlreadyTaken(final int position) {
        return this.positions.containsKey(position);
    }

    public boolean isWithinBounds(final int position) {
        return position >= 1 && position <= 9;
    }

    private boolean isWinning(final TicTacToePlayer player) {
        final Set<Integer> playersPositions = this.positions.entrySet()
                .stream()
                .filter(e -> e.getValue().equals(player))
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
        return WINNING_COMBINATIONS.stream().anyMatch(playersPositions::containsAll);
    }

    public Optional<TicTacToePlayer> getWinner() {
        if(isWinning(this.player1)) {
            return Optional.of(this.player1);
        }
        else if(isWinning(this.player2)) {
            return Optional.of(this.player2);
        }
        else {
            return Optional.empty();
        }
    }

    public boolean isDraw() {
        return this.positions.size() == 9;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        for(int i = 0; i < 3; i++) {
            for(int j = 1; j <= 3; j++) {
                final int currentPosition = i * 3 + j;
                final TicTacToePlayer positionOwner = this.positions.get(currentPosition);
                if(this.player1.equals(positionOwner)) {
                    builder.append("X");
                }
                else if(this.player2.equals(positionOwner)) {
                    builder.append("O");
                }
                else {
                    builder.append(" ");
                }
                if(j != 3) {
                    builder.append("|");
                }
            }
            builder.append("\n");
        }
        return builder.toString();
    }
}
