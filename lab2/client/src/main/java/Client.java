import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class Client {

    public static void main(final String[] args) {
        if(args.length != 3) {
            System.err.println("Invalid arguments. Usage: Client <host> <port> <nickname>");
            System.exit(1);
        }
        try {
            new Player(args[0], Integer.parseInt(args[1]), args[2]).start();
        }
        catch(final NumberFormatException e) {
            System.err.println("Invalid port number");
            System.exit(1);
        }
        catch(final RemoteException | NotBoundException | MalformedURLException e) {
            System.out.println("Fatal remote exception occurred. Will now terminate");
            System.exit(1);
        }
    }
}
