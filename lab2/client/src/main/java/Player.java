import exception.NicknameAlreadyTakenException;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;

public class Player implements TicTacToePlayer {
    private final Scanner scanner = new Scanner(System.in);
    private final String nickname;
    private final TicTacToeServer server;
    private char mark;

    public Player(final String host, final int port, final String nickname)
            throws RemoteException, MalformedURLException, NotBoundException {
        this.nickname = nickname;
        UnicastRemoteObject.exportObject(this, 0);
        this.server = (TicTacToeServer) Naming.lookup(String.format("rmi://%s:%d/server", host, port));
    }

    @Override
    public int onChoosePosition() throws RemoteException {
        System.out.println("Please choose your next move: (" + this.mark + "s are yours)");
        final int position = this.scanner.nextInt();
        return position;
    }

    @Override
    public void onOpponentMove(final Board board) throws RemoteException {
        System.out.println("Your opponent has chosen his move. The board is now as follows: ("
                + this.mark + "s are yours)");
        System.out.println(board.toString());
    }

    @Override
    public void onLoose() throws RemoteException {
        System.out.println("You loose!");
        finish();
    }

    @Override
    public void onWin() throws RemoteException {
        System.out.println("You win!");
        finish();
    }

    @Override
    public void onInvalidPositionChosen() throws RemoteException {
        System.out.println("You have chosen an invalid position!");
    }

    @Override
    public void onAlreadyTakenPositionChosen() throws RemoteException {
        System.out.println("You have chosen an already taken position!");
    }

    @Override
    public String nickname() throws RemoteException {
        return this.nickname;
    }

    @Override
    public void onError() {
        System.out.println("An error occurred during your game. Exiting!");
        finish();
    }

    @Override
    public void onDraw() throws RemoteException {
        System.out.println("It's a draw!");
        finish();
    }

    @Override
    public void onGameInitialized(final char mark, final String opponent) throws RemoteException {
        System.out.println("You will be playing against: " + opponent);
        System.out.println("You were assigned: " + mark + "s");
        this.mark = mark;
    }

    public void start() throws RemoteException {
        final GameType gameType = chooseGameType();
        showTutorial();
        System.out.println("Initializing your game...");
        try {
            this.server.join(this, gameType);
        }
        catch(final NicknameAlreadyTakenException e) {
            System.err.println("Nickname already taken. Please start with different one");
            System.exit(1);
        }
    }

    private GameType chooseGameType() {
        System.out.println("Type \"h\" to play against human or \"c\" to play against computer:");
        final String input = this.scanner.nextLine().toLowerCase();
        switch(input) {
            case "h":
                return GameType.PVP;
            case "c":
                return GameType.PVC;
            default:
                System.out.println("Invalid choice!");
                return chooseGameType();
        }
    }

    private void showTutorial() {
        System.out.println("You will be playing a TicTacToe. The board is as follows:");
        System.out.println("1|2|3\n" + "4|5|6\n" + "7|8|9");
    }

    private void finish() {
        new Thread(() -> System.exit(0)).start();
    }
}
