import java.util.Collection;

public interface GameFinishedListener {
    void onGameFinished(final Collection<TicTacToePlayer> players);
}
