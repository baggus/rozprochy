import java.rmi.RemoteException;
import java.util.Random;

public class ComputerPlayer implements TicTacToePlayer {

    public static final String NAME = "Computer";

    private static final Random random = new Random();

    @Override
    public int onChoosePosition() throws RemoteException {
        return random.nextInt(9) + 1;
    }

    @Override
    public void onOpponentMove(final Board board) throws RemoteException {
    }

    @Override
    public void onLoose() throws RemoteException {
    }

    @Override
    public void onWin() throws RemoteException {
    }

    @Override
    public void onDraw() throws RemoteException {
    }

    @Override
    public void onInvalidPositionChosen() throws RemoteException {
    }

    @Override
    public void onAlreadyTakenPositionChosen() throws RemoteException {
    }

    @Override
    public String nickname() throws RemoteException {
        return NAME;
    }

    @Override
    public void onError() throws RemoteException {
    }

    @Override
    public void onGameInitialized(final char mark, final String opponent) throws RemoteException {
    }
}
