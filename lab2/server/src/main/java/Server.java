import exception.NicknameAlreadyTakenException;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

public class Server implements TicTacToeServer, GameFinishedListener {

    private static final ComputerPlayer COMPUTER_PLAYER = new ComputerPlayer();

    private final Queue<TicTacToePlayer> awaitingPlayers = new ArrayDeque<>();
    private final Set<String> nicknamesUsed = Sets.newConcurrentHashSet();
    private final Map<TicTacToePlayer, String> playersNames = Maps.newConcurrentMap();

    private final ExecutorService executor = Executors.newFixedThreadPool(3);

    @Override
    public void join(final TicTacToePlayer player,
                     final GameType gameType) throws RemoteException, NicknameAlreadyTakenException {
        final String nickname = player.nickname();
        if(COMPUTER_PLAYER.nickname().equals(nickname) || !this.nicknamesUsed.add(nickname)) {
            throw new NicknameAlreadyTakenException();
        }
        this.playersNames.put(player, nickname);
        switch(gameType) {
            case PVC:
                initGameAgainstComputer(player);
                break;
            case PVP:
                initGameAgainstHuman(player);
                break;
        }
    }

    private void initGameAgainstComputer(final TicTacToePlayer player) {
        this.executor.execute(new Game(player, COMPUTER_PLAYER, this));
    }

    private void initGameAgainstHuman(final TicTacToePlayer player) {
        synchronized(this.awaitingPlayers) {
            if(!this.awaitingPlayers.isEmpty()) {
                final TicTacToePlayer opponent = this.awaitingPlayers.poll();
                this.executor.execute(new Game(player, opponent, this));
            }
            else {
                this.awaitingPlayers.add(player);
            }
        }
    }

    @Override
    public void onGameFinished(final Collection<TicTacToePlayer> players) {
        this.nicknamesUsed.removeAll(players.stream().map(this.playersNames::get).collect(Collectors.toList()));
        players.stream().forEach(this.playersNames::remove);
    }

    public static void main(final String[] args) throws RemoteException, MalformedURLException {
        if(args.length != 2) {
            System.err.println("Invalid arguments. Usage: Client <host> <port>");
            System.exit(1);
        }
        final TicTacToeServer server = new Server();
        UnicastRemoteObject.exportObject(server, 0);
        UnicastRemoteObject.exportObject(COMPUTER_PLAYER, 0);
        Naming.rebind(String.format("rmi://%s:%d/server", args[0], Integer.parseInt(args[1])), server);
    }
}
