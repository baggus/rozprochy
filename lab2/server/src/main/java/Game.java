import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class Game implements Runnable {

    private final List<TicTacToePlayer> players = new ArrayList<>();
    private final GameFinishedListener listener;

    public Game(final TicTacToePlayer player1, final TicTacToePlayer player2, final GameFinishedListener listener) {
        this.players.add(player1);
        this.players.add(player2);
        this.listener = listener;
    }

    @Override
    public void run() {
        final Board board = new Board(this.players.get(0), this.players.get(1));
        try {
            this.players.get(0).onGameInitialized('X', this.players.get(1).nickname());
            this.players.get(1).onGameInitialized('O', this.players.get(0).nickname());
            while(!board.getWinner().isPresent() && !board.isDraw()) {
                final TicTacToePlayer player1 = this.players.get(0);
                final TicTacToePlayer player2 = this.players.get(1);
                final int playersChoice = player1.onChoosePosition();
                if(board.isAlreadyTaken(playersChoice)) {
                    player1.onAlreadyTakenPositionChosen();
                }
                else if(!board.isWithinBounds(playersChoice)) {
                    player1.onInvalidPositionChosen();
                }
                else {
                    board.markPlayersPosition(player1, playersChoice);
                    player2.onOpponentMove(board);
                    Collections.reverse(this.players);
                }
            }
            final Optional<TicTacToePlayer> winner = board.getWinner();
            if(winner.isPresent()) {
                winner.get().onWin();
                this.players.stream().filter(p -> !p.equals(winner.get())).findFirst().get().onLoose();
            }
            else {
                this.players.get(0).onDraw();
                this.players.get(1).onDraw();
            }
        }
        catch(final RemoteException e) {
            System.err.println("RemoteException occurred. Exiting game!");
            for(final TicTacToePlayer player : this.players) {
                try {
                    player.onError();
                }
                catch(final RemoteException ignored) {
                }
            }
        }
        finally {
            this.listener.onGameFinished(this.players);
        }
    }
}
