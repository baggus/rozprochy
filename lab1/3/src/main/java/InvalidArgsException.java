public class InvalidArgsException extends Exception {

    public InvalidArgsException(final String message) {
        super(message);
    }
}
