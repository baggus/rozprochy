import java.net.InetAddress;
import java.net.UnknownHostException;

public class Main {

    public static void main(final String[] args) {
        try {
            final int port = getPortFromArgs(args);
            final InetAddress ipAddress = getIpAddressFromArgs(args);
            final String nick = getNameFromArgs(args);
            new ChatClient(ipAddress, port, nick).start();
        }
        catch(final InvalidArgsException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }
    }

    private static String getNameFromArgs(final String[] args) throws InvalidArgsException {
        if(args.length < 3) {
            throw new InvalidArgsException("Please provide nick");
        }
        final String nick = args[2];
        if(nick.length() > 6) {
            throw new InvalidArgsException("Nick must be 6 characters long. Given: " + nick.length());
        }
        return nick;
    }

    private static int getPortFromArgs(final String[] args) throws InvalidArgsException {
        if(args.length < 2) {
            throw new InvalidArgsException("Please provide port number");
        }
        return Integer.parseInt(args[1]);
    }

    private static InetAddress getIpAddressFromArgs(final String[] args) throws InvalidArgsException {
        if(args.length < 1) {
            throw new InvalidArgsException("Please provide ip address");
        }
        try {
            return InetAddress.getByName(args[0]);
        }
        catch(final UnknownHostException e) {
            throw new InvalidArgsException("Incorrect ip address");
        }
    }
}
