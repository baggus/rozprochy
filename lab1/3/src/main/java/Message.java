import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.zip.CRC32;

import com.google.common.base.Objects;

public class Message {
    /**
     * In Bytes
     */
    public static final int MAX_SIZE = 110;

    private final String nick;
    private final LocalDateTime sentTime;
    private final String msg;

    public Message(final String nick, final String msg) {
        this(nick, msg, LocalDateTime.now());
    }

    public Message(final String nick, final String msg, final LocalDateTime sentTime) {
        this.nick = nick;
        this.msg = trim(msg);
        this.sentTime = sentTime.truncatedTo(ChronoUnit.SECONDS);
    }

    public String nick() {
        return this.nick;
    }

    @Override
    public boolean equals(final Object o) {
        if(this == o) {
            return true;
        }
        if(o == null || getClass() != o.getClass()) {
            return false;
        }
        final Message message = (Message) o;
        return Objects.equal(this.nick, message.nick) &&
                Objects.equal(this.sentTime, message.sentTime) &&
                Objects.equal(this.msg, message.msg);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.nick, this.sentTime, this.msg);
    }

    @Override
    public String toString() {
        return this.sentTime + " " + this.nick + ": " + this.msg;
    }

    private static String trim(final String message) {
        if(message.length() > 20) {
            return message.substring(0, 20);
        }
        return message;
    }

    /**
     * 4B - nick length
     * (max) 6*2B - nick
     * 4B - sentTime length
     * 19*2B - sentTime
     * 4B - msg length
     * (max) 20*2B - msg
     * 8B - checksum
     */

    private static final int CONSTANT_SIZE = 4 + 4 + 19 * 2 + 4 + 8;

    public byte[] toRawData() throws IOException {
        final ByteArrayOutputStream b = new ByteArrayOutputStream(MAX_SIZE);
        final DataOutputStream d = new DataOutputStream(b);
        int realSize = CONSTANT_SIZE;
        d.writeInt(this.nick.length());
        realSize += this.nick.length() * 2;
        d.writeChars(this.nick);
        d.writeInt(this.sentTime.toString().length());
        d.writeChars(this.sentTime.toString());
        d.writeInt(this.msg.length());
        d.writeChars(this.msg);
        realSize += this.msg.length() * 2;
        d.writeLong(checkSum());
        return Arrays.copyOf(b.toByteArray(), realSize);
    }

    public static Message fromRawData(final byte[] rawData) throws IOException, InvalidChecksumException {
        final ByteArrayInputStream b = new ByteArrayInputStream(rawData);
        final DataInputStream d = new DataInputStream(b);
        final int nicknameLength = d.readInt();
        StringBuilder builder = new StringBuilder(nicknameLength);
        for(int i = 0; i < nicknameLength; i++) {
            builder.append(d.readChar());
        }
        final String nickname = builder.toString();
        final int timeLength = d.readInt();
        builder = new StringBuilder(timeLength);
        for(int i = 0; i < timeLength; i++) {
            builder.append(d.readChar());
        }
        final LocalDateTime time = LocalDateTime.parse(builder.toString());
        final int msgLength = d.readInt();
        builder = new StringBuilder(msgLength);
        for(int i = 0; i < msgLength; i++) {
            builder.append(d.readChar());
        }
        final String msg = builder.toString();
        final Message message = new Message(nickname, msg, time);
        final long checkSum = d.readLong();
        if(checkSum != message.checkSum()) {
            throw new InvalidChecksumException();
        }
        return message;
    }

    private long checkSum() {
        final CRC32 crc = new CRC32();
        crc.update(this.nick.getBytes());
        crc.update(this.sentTime.toString().getBytes());
        crc.update(this.msg.getBytes());
        return crc.getValue();
    }
}
