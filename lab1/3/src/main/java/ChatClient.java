import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketTimeoutException;
import java.util.Scanner;

public class ChatClient {
    private final InetAddress ipAddress;
    private final String nick;
    private final int port;

    public ChatClient(final InetAddress ipAddress, final int port, final String nick) {
        this.ipAddress = ipAddress;
        this.port = port;
        this.nick = nick;
    }

    public void start() {
        startSendingThread();
        startReceivingThread();
    }

    private void startSendingThread() {
        final Runnable r = () -> {
            try {
                final Scanner scanner = new Scanner(System.in);
                final MulticastSocket socket = new MulticastSocket(this.port);
                while(true) {
                    final String input = scanner.nextLine();
                    final Message message = new Message(this.nick, input);
                    final byte[] rawData = message.toRawData();
                    final DatagramPacket datagramPacket = new DatagramPacket(rawData,
                            rawData.length,
                            this.ipAddress,
                            this.port);
                    socket.send(datagramPacket);
                }
            }
            catch(final Exception e) {
                System.err.println(e.getMessage());
            }
        };
        new Thread(r).start();
    }

    private void startReceivingThread() {
        final Runnable r = () -> {
            try {
                final MulticastSocket socket = new MulticastSocket(this.port);
                socket.joinGroup(this.ipAddress);
                socket.setSoTimeout(5000);
                while(true) {
                    try {
                        final DatagramPacket packet = new DatagramPacket(new byte[Message.MAX_SIZE], Message.MAX_SIZE);
                        socket.receive(packet);
                        final Message msg = Message.fromRawData(packet.getData());
                        if(!msg.nick().equals(this.nick)) {
                            System.out.println(msg.toString());
                        }
                    }
                    catch(final SocketTimeoutException ignored) {
                    }
                    catch(final InvalidChecksumException e) {
                        System.out.println("Invalid message checksum");
                    }
                }
            }
            catch(Exception e) {
                System.err.println(e.getMessage());
            }
        };
        new Thread(r).start();
    }
}
