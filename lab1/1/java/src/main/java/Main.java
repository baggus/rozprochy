import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Main {

    private final int port;

    public Main(final int port) {
        this.port = port;
    }

    private void run() {
        try {
            final ServerSocket serverSocket = new ServerSocket(this.port);
            final Socket clientSocket = serverSocket.accept();
            final DataOutputStream out = new DataOutputStream(clientSocket.getOutputStream());
            final DataInputStream in = new DataInputStream(clientSocket.getInputStream());
            while(true) {
                final long request = parseInput(in);
                System.out.println("Got: " + request);
                final byte result = new Bpp().getDecimal(request);
                System.out.println("Sent: " + result);
                out.write(result);
            }
        }
        catch(final EOFException e) {
            System.out.println("End of stream");
        }
        catch(final IOException e) {
            System.err.println("Something went terribly wrong!");
            e.printStackTrace();
        }
    }

    private long parseInput(final DataInputStream in) throws IOException {
        final int size = in.readByte();
        switch(size) {
            case 1:
                return in.readByte();
            case 2:
                return in.readShort();
            case 4:
                return in.readInt();
            case 8:
                return in.readLong();
            default:
                throw new IllegalStateException("Illegal size: " + size);
        }
    }

    public static void main(final String[] args) {
        if(args.length != 1) {
            System.err.println("Please provide port number");
            System.exit(1);
        }
        final int port = Integer.parseInt(args[0]);
        new Main(port).run();
    }
}
