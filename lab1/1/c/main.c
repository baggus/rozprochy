#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdlib.h>
#include <arpa/inet.h>

#define TYP_INIT 0
#define TYP_SMLE 1
#define TYP_BIGE 2

unsigned long long int htonll(unsigned long long int src) {
static int typ = TYP_INIT;
unsigned char c;
union {
    unsigned long long ull;
    unsigned char c[8];
} x;
if (typ == TYP_INIT) {
x.
ull = 0x01;
typ = (x.c[7] == 0x01ULL) ? TYP_BIGE : TYP_SMLE;
}
if (typ == TYP_BIGE)
return
src;
x.
ull = src;
c = x.c[0];
x.c[0] = x.c[7];
x.c[7] =
c;
c = x.c[1];
x.c[1] = x.c[6];
x.c[6] =
c;
c = x.c[2];
x.c[2] = x.c[5];
x.c[5] =
c;
c = x.c[3];
x.c[3] = x.c[4];
x.c[4] =
c;
return x.
ull;
}

int sock_fd;

void connect_to_server(char *ip_address, char *port) {
    if ((sock_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("Cannot create socket");
        exit(1);
    }
    struct sockaddr_in serv_addr;
    bzero(&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(ip_address);
    serv_addr.sin_port = htons(atoi(port));
    if (connect(sock_fd, (struct sockaddr *) &serv_addr, sizeof(struct sockaddr_in)) == -1) {
        perror("Cannot connect to server");
        exit(1);
    }
}

void push(void *to_send, size_t to_send_size){
    if (send(sock_fd, to_send, to_send_size, 0) < 0) {
        perror("Cannot send to server");
        exit(1);
    }
}

void send_to_server(long long int request) {
    int8_t size;
    void *data;
    if (request <= ((1 << 7) - 1)) {
        size = (int8_t) 1;
        int8_t converted = (int8_t) request;
        data = &converted;
    } else if (request <= (1 << 15) - 1) {
        size = (int8_t) 2;
        int16_t converted = (int16_t) htons(request);
        data = &converted;
    } else if (request <= (1 << 31) - 1) {
        size = (int8_t) 4;
        int32_t converted = (int32_t) htonl(request);
        data = &converted;
    } else if (request <= (1 << 63) - 1) {
        size = (int8_t) 8;
        int64_t converted = (int64_t) htonll(request);
        data = &converted;
    } else {
        printf("Number is too big!");
        exit(1);
    }
    push(&size, 1);
    push(data, (size_t) size);
}

char receive_from_server() {
    char digit;
    if (recv(sock_fd, (void *) &digit, sizeof(char), 0) < 0) {
        perror("Cannot receive from server");
        exit(1);
    }
    return digit;
}

int main(int argc, char **argv) {

    if (argc != 3) {
        printf("Please provide server ip address and port number\n");
        exit(1);
    }
    connect_to_server(argv[1], argv[2]);
    while (1) {
        long long int n;
        printf("Enter digit index (or 0 to finish): \n");
        scanf("%lld", &n);
        if (n == 0) {
            break;
        } else if (n < 0) {
            printf("You have entered a negative index!");
        } else {
            send_to_server(n);
            char response = receive_from_server();
            printf("%d\n", response);
        }
    }
    close(sock_fd);
    return 0;
}