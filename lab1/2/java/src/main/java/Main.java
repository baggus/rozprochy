import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {

    private final int port;

    public Main(final int port) {
        this.port = port;
    }

    public void run() {
        try {
            final ServerSocket serverSocket = new ServerSocket(this.port);
            final Socket clientSocket = serverSocket.accept();
            final DataInputStream socketInput = new DataInputStream(clientSocket.getInputStream());
            final String fileName = readFilename(socketInput);
            final File file = openFile(fileName);
            copyDataFromSocketToFile(socketInput, file);
        }
        catch(final EOFException e) {
            System.out.println("End of stream!");
        }
        catch(final IOException e) {
            System.err.println("Something went terribly wrong!");
            e.printStackTrace();
        }
    }

    private String readFilename(final DataInputStream in) throws IOException {
        final byte length = in.readByte();
        final StringBuilder builder = new StringBuilder(length);
        for(int i = 0; i < length; i++) {
            final char c = (char) in.readByte();
            builder.append(c);
        }
        return builder.toString();
    }

    private File openFile(final String fileName) throws IOException {
        final File folder = new File("client_folder");
        if(!folder.exists()) {
            folder.mkdir();
        }
        final Path path = Paths.get("client_folder", fileName);
        final File file = new File(path.toString());
        if(!file.exists()) {
            file.createNewFile();
        }
        return file;
    }

    private void copyDataFromSocketToFile(final DataInputStream socketInput, final File file) throws IOException {
        final DataOutputStream fileOutput = new DataOutputStream(new FileOutputStream(file));
        int data;
        while((data = socketInput.read()) != -1) {
            fileOutput.write(data);
        }
    }

    public static void main(final String[] args) {
        if(args.length != 1) {
            System.err.println("Please provide port number");
            System.exit(1);
        }
        final int port = Integer.parseInt(args[0]);
        new Main(port).run();
    }
}
