#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/sendfile.h>
#include <sys/stat.h>

int sock_fd;

void connect_to_server(char *ip_address, char *port) {
    if ((sock_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("Cannot create socket");
        exit(1);
    }
    struct sockaddr_in serv_addr;
    bzero(&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(ip_address);
    serv_addr.sin_port = htons(atoi(port));
    if (connect(sock_fd, (struct sockaddr *) &serv_addr, sizeof(struct sockaddr_in)) == -1) {
        perror("Cannot connect to server");
        exit(1);
    }
}

void send_file(char *file_name) {
    FILE *to_send = fopen(file_name, "r");
    if (!to_send) {
        perror("Cannot open file");
        exit(1);
    }
    int8_t file_name_length = (int8_t) strlen(file_name);
    char header[file_name_length + 1];
    header[0] = file_name_length;
    sprintf(header + 1, "%s", file_name);
    if (send(sock_fd, &header, sizeof(header), 0) < 0) {
        perror("Cannot send to server");
        exit(1);
    }
    struct stat file_info;
    if (stat(file_name, &file_info) < 0) {
        perror("Cannot access file information");
        exit(1);
    }
    off_t offset = 0;
    int sent = 0;
    off_t file_size = file_info.st_size;
    off_t remaining = file_size;
    while (((sent = sendfile(sock_fd, fileno(to_send), &offset, BUFSIZ)) > 0) && (remaining > 0)) {
        remaining -= sent;
        printf("Progress: %f %%\n", (1 - (double)remaining/(double)file_size) * 100);
    }

    if (fclose(to_send) == EOF) {
        perror("Error while closing file");
        exit(1);
    }
}

int main(int argc, char **argv) {
    if (argc != 4) {
        printf("Please provide ip address, port number and file");
        exit(1);
    }
    connect_to_server(argv[1], argv[2]);
    send_file(argv[3]);
    close(sock_fd);
    return 0;
}