
#ifndef SR_DEMO_ICE
#define SR_DEMO_ICE

module Demo
{
	exception RequestCanceledException
	{
	};

	interface Temperature
	{
		int get();
	};

	interface Noteboard
	{
	    string getText();
	    void append(string text);
	};

	interface Login
	{
	    bool authenticate(string password);
	};

	interface Counter
	{
	    int increase();
	};

	interface Calc
	{
	    int add(int a, int b);
	};

};

#endif
