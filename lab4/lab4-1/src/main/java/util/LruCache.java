package util;

import java.util.LinkedHashMap;
import java.util.Map;

public class LruCache<K, V> extends LinkedHashMap<K, V> {

    private final int maxSize;
    private final RemovalListener<K, V> listener;

    public LruCache(final int maxSize, final RemovalListener<K, V> listener) {
        super(maxSize / 2, 0.75f, true);
        this.maxSize = maxSize;
        this.listener = listener;
    }

    @Override
    protected boolean removeEldestEntry(final Map.Entry<K, V> eldest) {
        final boolean shouldRemove = this.size() > this.maxSize;
        if(shouldRemove) {
            this.listener.onRemove(eldest.getKey(), eldest.getValue());
        }
        return shouldRemove;
    }

    public interface RemovalListener<K, V> {
        void onRemove(final K key, final V value);
    }
}
