
//K1

package clients;

import Demo.LoginPrx;
import Demo.LoginPrxHelper;
import Ice.Communicator;
import Ice.ObjectPrx;
import Ice.Util;

import java.util.Scanner;

public class LoginClient {
    public static void main(final String[] args) {
        final Communicator communicator = Util.initialize();
        final Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your id: ");
        final String id = scanner.nextLine().trim();
        final ObjectPrx base1 = communicator.stringToProxy(
                "login/" + id + ":tcp -h localhost -p 10000:udp -h localhost -p 10000:ssl -h localhost -p 10001");
        final LoginPrx login = LoginPrxHelper.checkedCast(base1);
        while(true) {
            System.out.println("Enter password: ");
            final String password = scanner.nextLine();
            if(login.authenticate(password)) {
                System.out.println("Password correct!");
                System.exit(0);
            }
        }
    }
}
