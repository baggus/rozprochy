
//K4

package clients;

import Demo.TemperaturePrx;
import Demo.TemperaturePrxHelper;
import Ice.Communicator;
import Ice.ObjectPrx;
import Ice.Util;

public class TemperatureClient {
    public static void main(final String[] args) {
        final Communicator communicator = Util.initialize();
        final ObjectPrx base1 = communicator.stringToProxy(
                "temperature/_:tcp -h localhost -p 10000:udp -h localhost -p 10000:ssl -h localhost -p 10001");
        final TemperaturePrx temperature = TemperaturePrxHelper.checkedCast(base1);
        for(int i = 0; i < 10; i++) {
            System.out.println("Current temperature: " + temperature.get());
        }
    }
}
