
//K2

package clients;

import Demo.CounterPrx;
import Demo.CounterPrxHelper;
import Ice.Communicator;
import Ice.ObjectPrx;
import Ice.Util;

public class CounterClient {

    public static void main(final String[] args) {
        final Communicator communicator = Util.initialize();
        final ObjectPrx base1 = communicator.stringToProxy(
                "counter/_:tcp -h localhost -p 10000:udp -h localhost -p 10000:ssl -h localhost -p 10001");
        final CounterPrx counter = CounterPrxHelper.checkedCast(base1);
        for(int i = 0; i < 10; i++) {
            System.out.println("This servant has responded " + counter.increase() + " time(s) so far");
        }
    }
}
