
//K5

package clients;

import Demo.NoteboardPrx;
import Demo.NoteboardPrxHelper;
import Ice.Communicator;
import Ice.ObjectPrx;
import Ice.Util;

import java.util.Scanner;

public class NoteboardClient {
    public static void main(final String[] args) {
        final Communicator communicator = Util.initialize();
        final Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your id: ");
        final String id = scanner.nextLine().trim();
        final ObjectPrx base1 = communicator.stringToProxy(
                "noteboard/" + id + ":tcp -h localhost -p 10000:udp -h localhost -p 10000:ssl -h localhost -p 10001");
        final NoteboardPrx noteboard = NoteboardPrxHelper.checkedCast(base1);
        System.out.println("Usage:");
        System.out.println("append <text>");
        System.out.println("display");
        System.out.println("exit");
        while(true) {
            final String[] line = scanner.nextLine().split(" ");
            if("exit".equals(line[0])) {
                System.exit(0);
            }
            else if("append".equals(line[0])) {
                noteboard.append(line[1]);
            }
            else if("display".equals(line[0])) {
                System.out.println(noteboard.getText());
            }
            else {
                System.out.println("Invalid usage...");
            }
        }
    }
}
