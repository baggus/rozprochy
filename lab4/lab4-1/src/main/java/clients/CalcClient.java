
//K3

package clients;

import Demo.CalcPrx;
import Demo.CalcPrxHelper;
import Ice.Communicator;
import Ice.ObjectPrx;
import Ice.Util;

import java.util.Scanner;

public class CalcClient {
    public static void main(final String[] args) {
        final Communicator communicator = Util.initialize();
        final Scanner scanner = new Scanner(System.in);
        final ObjectPrx base1 = communicator.stringToProxy(
                "calc/_:tcp -h localhost -p 10000:udp -h localhost -p 10000:ssl -h localhost -p 10001");
        final CalcPrx calc = CalcPrxHelper.checkedCast(base1);
        while(true) {
            System.out.println("Enter first number: ");
            final int a = Integer.parseInt(scanner.nextLine());
            System.out.println("Enter second number: ");
            final int b = Integer.parseInt(scanner.nextLine());
            System.out.println(calc.add(a, b));
        }
    }
}
