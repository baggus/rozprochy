import Ice.Communicator;
import Ice.ObjectAdapter;
import Ice.Util;
import servants.CalcServantLocator;
import servants.CounterServantLocator;
import servants.LoginServantLocator;
import servants.NoteboardServantEvictor;
import servants.TemperatureServant;

public class Server {

    public static void main(final String[] args) {
        final Communicator communicator = Util.initialize();
        final ObjectAdapter adapter = communicator.createObjectAdapterWithEndpoints("Adapter1",
                "tcp -h localhost -p 10000:udp -h localhost -p 10000");
        initLoginServantLocator(adapter); //K1
        initCounterServantLocator(adapter); //K2
        initCalcServantLocator(adapter); //K3
        adapter.addDefaultServant(new TemperatureServant(), "temperature"); // K4
        initNoteboardServantLocator(adapter); //K5
        adapter.activate();
        communicator.waitForShutdown();
    }

    private static void initNoteboardServantLocator(final ObjectAdapter adapter) {
        final NoteboardServantEvictor locator = new NoteboardServantEvictor(2);
        adapter.addServantLocator(locator, "noteboard");
    }

    private static void initCalcServantLocator(final ObjectAdapter adapter) {
        final CalcServantLocator locator = new CalcServantLocator(3);
        adapter.addServantLocator(locator, "calc");
    }

    private static void initLoginServantLocator(final ObjectAdapter adapter) {
        final LoginServantLocator locator = new LoginServantLocator(adapter);
        adapter.addServantLocator(locator, "login");
    }

    private static void initCounterServantLocator(final ObjectAdapter adapter) {
        final CounterServantLocator locator = new CounterServantLocator();
        adapter.addServantLocator(locator, "counter");
    }
}
