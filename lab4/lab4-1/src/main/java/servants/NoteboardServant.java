package servants;

import Demo._NoteboardDisp;
import Ice.Current;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;

public class NoteboardServant extends _NoteboardDisp implements Saveable {

    private final StringBuffer buffer;

    public NoteboardServant(final String initialState) {
        this.buffer = new StringBuffer(initialState);
    }

    @Override
    public String getText(final Current __current) {
        return this.buffer.toString();
    }

    @Override
    public void append(final String text, final Current __current) {
        this.buffer.append(text).append("\n");
    }

    @Override
    public void saveTo(final BufferedOutputStream out) throws IOException {
        out.write(this.buffer.toString().getBytes(Charset.defaultCharset()));
    }
}
