package servants;

import Ice.Current;
import Ice.Identity;
import Ice.LocalObjectHolder;
import Ice.Object;
import Ice.ObjectAdapter;
import Ice.ServantLocator;
import Ice.UserException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class LoginServantLocator implements ServantLocator {

    private final ObjectAdapter adapter;

    public LoginServantLocator(final ObjectAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public Object locate(final Current current, final LocalObjectHolder localObjectHolder) throws UserException {
        final String id = current.id.name;
        try {
            final LoginServant servant = new LoginServant(loadPassword(id));
            this.adapter.add(servant, new Identity(id, "login"));
            System.out.println("Created and registered new servant for client: " + id);
            return servant;
        }
        catch(final FileNotFoundException ignored) {
        }
        return null;
    }

    @Override
    public void finished(final Current current, final Object object, final java.lang.Object o) throws UserException {
    }

    @Override
    public void deactivate(final String s) {
    }

    private static String loadPassword(final String id) throws FileNotFoundException {
        final File file = new File("passwords.txt");
        return new BufferedReader(new FileReader(file)).lines()
                .map(line -> line.split(";"))
                .filter(line -> id.equals(line[0]))
                .map(line -> line[1])
                .findFirst()
                .get();
    }
}
