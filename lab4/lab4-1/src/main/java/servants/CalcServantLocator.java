package servants;

import Ice.Current;
import Ice.LocalObjectHolder;
import Ice.Object;
import Ice.ServantLocator;
import Ice.UserException;

import java.util.ArrayDeque;
import java.util.Queue;

public class CalcServantLocator implements ServantLocator {

    private final Queue<CalcServant> servantsQueue = new ArrayDeque<>();

    public CalcServantLocator(final int numberOfServants) {
        for(int i = 0; i < numberOfServants; i++) {
            this.servantsQueue.add(new CalcServant());
        }
    }

    @Override
    public Object locate(final Current current, final LocalObjectHolder localObjectHolder) throws UserException {
        final CalcServant servant = this.servantsQueue.poll();
        this.servantsQueue.add(servant);
        return servant;
    }

    @Override
    public void finished(final Current current, final Object object, final java.lang.Object o) throws UserException {
    }

    @Override
    public void deactivate(final String s) {
    }
}
