package servants;

import Demo._TemperatureDisp;
import Ice.Current;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class TemperatureServant extends _TemperatureDisp {

    private static final AtomicInteger GLOBAL_ID = new AtomicInteger();
    private static final Random RANDOM = new Random();

    private final int id = GLOBAL_ID.getAndDecrement();

    @Override
    public int get(final Current __current) {
        final int temp = RANDOM.nextInt(40);
        System.out.println("Temperature servant " + this.id + " replies");
        return temp;
    }

    @Override
    public String toString() {
        return "servants.TemperatureServant-" + this.id;
    }
}
