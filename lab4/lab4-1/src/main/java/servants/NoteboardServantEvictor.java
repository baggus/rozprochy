package servants;

import Ice.Current;
import Ice.LocalObjectHolder;
import Ice.Object;
import Ice.ServantLocator;
import Ice.UserException;
import util.LruCache;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.stream.Collectors;

import com.google.common.base.Joiner;

public class NoteboardServantEvictor implements ServantLocator {

    private final LruCache<String, NoteboardServant> cache;

    public NoteboardServantEvictor(final int cacheSize) {
        this.cache = new LruCache<>(cacheSize, new RemovalListener());
    }

    @Override
    public Object locate(final Current current, final LocalObjectHolder localObjectHolder) throws UserException {
        final String clientName = current.id.name;
        if(this.cache.containsKey(clientName)) {
            System.out.println("NoteboardServantEvictor: returning servant from cache");
            return this.cache.get(clientName);
        }
        else {
            System.out.println("NoteboardServantEvictor: servant not found in cache. Creating new one.");
            final NoteboardServant servant;
            servant = new NoteboardServant(loadInitialState(clientName));
            this.cache.put(clientName, servant);
            return servant;
        }
    }

    private static String loadInitialState(final String clientName) {
        final File file = new File("noteboard_" + clientName);
        if(!file.exists()) {
            return "";
        }
        try {
            return Joiner.on("\n").join(new BufferedReader(new FileReader(file)).lines().collect(Collectors.toList()));
        }
        catch(final FileNotFoundException ignore) {
        }
        return "";
    }

    @Override
    public void finished(final Current current, final Object object, final java.lang.Object o) throws UserException {
    }

    @Override
    public void deactivate(final String s) {
    }

    private static class RemovalListener implements LruCache.RemovalListener<String, NoteboardServant> {

        @Override
        public void onRemove(final String key, final NoteboardServant value) {
            System.out.println("NoteboardServantEvictor: Servant removed from cache. Saving its state.");
            final File file = new File("noteboard_" + key);
            try {
                if(!file.exists()) {
                    file.createNewFile();
                }
                final BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(file));
                value.saveTo(out);
                out.close();
            }
            catch(final IOException e) {
                e.printStackTrace();
            }
        }
    }
}
