package servants;

import Demo._CounterDisp;
import Ice.Current;

import java.util.concurrent.atomic.AtomicInteger;

public class CounterServant extends _CounterDisp {

    private static final AtomicInteger ID = new AtomicInteger();

    private final int id = ID.getAndIncrement();

    private int counter;

    @Override
    public int increase(final Current __current) {
        System.out.println("servants.CounterServant: " + this.id + " replies");
        return ++this.counter;
    }
}
