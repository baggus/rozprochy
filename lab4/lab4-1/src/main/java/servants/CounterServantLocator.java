package servants;

import Ice.Current;
import Ice.LocalObjectHolder;
import Ice.Object;
import Ice.ServantLocator;
import Ice.UserException;

public class CounterServantLocator implements ServantLocator {

    @Override
    public Object locate(final Current current, final LocalObjectHolder localObjectHolder) throws UserException {
        return new CounterServant();
    }

    @Override
    public void finished(final Current current, final Object object, final java.lang.Object o) throws UserException {
    }

    @Override
    public void deactivate(final String s) {
    }
}
