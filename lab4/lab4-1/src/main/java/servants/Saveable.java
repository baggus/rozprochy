package servants;

import java.io.BufferedOutputStream;
import java.io.IOException;

public interface Saveable {
    void saveTo(final BufferedOutputStream out) throws IOException;
}
