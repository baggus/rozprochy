package servants;

import Demo._LoginDisp;
import Ice.Current;

import java.util.concurrent.atomic.AtomicInteger;

public class LoginServant extends _LoginDisp {

    private final String password;

    private static final AtomicInteger ID = new AtomicInteger();

    private final int id = ID.getAndIncrement();

    public LoginServant(final String password) {
        this.password = password;
    }

    @Override
    public boolean authenticate(final String password, final Current __current) {
        System.out.println("servants.LoginServant " + this.id + " replies");
        return this.password.equals(password);
    }
}
