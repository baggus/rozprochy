package servants;

import Demo._CalcDisp;
import Ice.Current;

import java.util.concurrent.atomic.AtomicInteger;

public class CalcServant extends _CalcDisp {

    private static final AtomicInteger ID = new AtomicInteger();

    private final int id = ID.getAndIncrement();

    @Override
    public int add(final int a, final int b, final Current __current) {
        System.out.println("CalcServant " + this.id + " replies");
        return a + b;
    }
}
