import java.io.IOException;
import java.util.Scanner;

import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;

public class Main {

    private static final String ZNODE_NAME = "/znode_testowy";

    private final ZooKeeper zooKeeper;

    public Main(final String[] args) throws IOException, KeeperException, InterruptedException {
        this.zooKeeper = new ZooKeeper("127.0.0.1:2181", 3000, null);
        final Watcher watcher = new EventsWatcher(args[0]);
        this.zooKeeper.exists(ZNODE_NAME, watcher);
        final Scanner scanner = new Scanner(System.in);
        while(true) {
            System.out.println("Type print to print znode_testowy tree");
            final String input = scanner.nextLine();
            if("print".equalsIgnoreCase(input)) {
                printTree(ZNODE_NAME, "");
            }
        }
    }

    public static void main(final String[] args) throws IOException, KeeperException, InterruptedException {
        new Main(args);
    }

    private class EventsWatcher implements Watcher {
        private final String processName;
        private Process process;

        public EventsWatcher(final String processName) {
            this.processName = processName;
        }

        @Override
        public void process(final WatchedEvent watchedEvent) {
            try {
                switch(watchedEvent.getType()) {
                    case NodeCreated:
                        Main.this.zooKeeper.getChildren(watchedEvent.getPath(), this);
                        this.process = Runtime.getRuntime().exec(this.processName);
                        break;
                    case NodeDeleted:
                        Main.this.zooKeeper.exists(watchedEvent.getPath(), this);
                        if(this.process != null) {
                            this.process.destroy();
                        }
                        this.process = null;
                        break;
                    case NodeChildrenChanged:
                        System.out.println("znode_testowy has " + Main.this.zooKeeper.getChildren(watchedEvent.getPath(),
                                this).size() + " children");
                        break;
                }
            }
            catch(final Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void printTree(final String path, final String indent) throws KeeperException, InterruptedException {
        System.out.println(indent + path);
        try {
            for(final String children : this.zooKeeper.getChildren(path, false)) {
                printTree(path + "/" + children, indent + "    ");
            }
        }
        catch(final KeeperException.NoNodeException e) {
            System.out.println("Node does not exist!");
        }
    }
}
