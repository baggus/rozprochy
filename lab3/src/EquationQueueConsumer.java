import solver.ParseException;
import solver.Solver;

import java.util.Map;
import java.util.concurrent.Executor;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.QueueConnection;
import javax.jms.QueueReceiver;
import javax.jms.TextMessage;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;

public class EquationQueueConsumer implements Runnable {

    private final QueueReceiver receiver;
    private final QueueConnection queueConnection;
    private final Solver solver;
    private final Map<String, TopicPublisher> publishers;
    private final TopicSession topicSession;
    private final Executor executor;

    public EquationQueueConsumer(final QueueReceiver receiver,
                                 final QueueConnection queueConnection, final Solver solver,
                                 final Map<String, TopicPublisher> publishers,
                                 final TopicSession topicSession, final Executor executor) {
        this.receiver = receiver;
        this.queueConnection = queueConnection;
        this.solver = solver;
        this.publishers = publishers;
        this.topicSession = topicSession;
        this.executor = executor;
    }

    @Override
    public void run() {
        try {
            this.receiver.setMessageListener(new Listener());
            this.queueConnection.start();
        }
        catch(final JMSException e) {
            throw new RuntimeException("JMS Error while starting consuming queue", e);
        }
    }

    private void publish(final TopicPublisher publisher, final Message message) {
        this.executor.execute(() -> {
            try {
                publisher.publish(message);
            }
            catch(final JMSException e) {
                e.printStackTrace();
            }
        });
    }

    private class Listener implements MessageListener {

        @Override
        public void onMessage(final Message message) {
            try {
                if(message instanceof TextMessage) {
                    final TextMessage textMessage = (TextMessage) message;
                    final String input = textMessage.getText();
                    final int output = EquationQueueConsumer.this.solver.solve(input);
                    final String op = input.split(" ")[1];
                    if(!EquationQueueConsumer.this.publishers.containsKey(op)) {
                        System.err.println("There is no publisher for this type of operation: " + op);
                    }
                    else {
                        final TextMessage outputMessage = EquationQueueConsumer.this.topicSession.createTextMessage();
                        final String outputMessageText = input + " = " + output;
                        outputMessage.setText(outputMessageText);
                        publish(EquationQueueConsumer.this.publishers.get(op), message);
                        System.out.println("Solved and published: " + outputMessageText);
                    }
                }
                else {
                    System.out.println("Unsupported message type: " + message.getJMSType());
                }
            }
            catch(final JMSException e) {
                throw new RuntimeException("JMS Error while consuming Queue", e);
            }
            catch(final ParseException | ArithmeticException e) {
                System.err.println("Receive invalid message. Dropping it...");
            }
        }
    }
}
