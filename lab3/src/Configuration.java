import solver.Solver;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executors;

import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.google.common.collect.ImmutableMap;

public class Configuration {

    private static final String JNDI_CONTEXT_FACTORY_CLASS_NAME = "org.exolab.jms.jndi.InitialContextFactory";
    private static final String DEFAULT_JMS_PROVIDER_URL = "tcp://localhost:3035/";
    private static final String EQUATION_QUEUE_NAME = "equation_queue";
    private static final String EQUATION_TOPIC_BASE_NAME = "equation_topic_";

    private final Context context;
    private final QueueConnectionFactory queueConnectionFactory;
    private final TopicConnectionFactory topicConnectionFactory;
    private final Map<String, Topic> topics;

    public Configuration() throws NamingException {
        final Properties props = new Properties();
        props.put(Context.INITIAL_CONTEXT_FACTORY, JNDI_CONTEXT_FACTORY_CLASS_NAME);
        props.put(Context.PROVIDER_URL, DEFAULT_JMS_PROVIDER_URL);
        this.context = new InitialContext(props);
        this.queueConnectionFactory = (QueueConnectionFactory) this.context.lookup("ConnectionFactory");
        this.topicConnectionFactory = (TopicConnectionFactory) this.context.lookup("ConnectionFactory");
        this.topics = createTopics();
    }

    private Map<String, Topic> createTopics() throws NamingException {
        return ImmutableMap.of(
                "+", (Topic) this.context.lookup(EQUATION_TOPIC_BASE_NAME + "plus"),
                "-", (Topic) this.context.lookup(EQUATION_TOPIC_BASE_NAME + "minus"),
                "*", (Topic) this.context.lookup(EQUATION_TOPIC_BASE_NAME + "multiply"),
                "/", (Topic) this.context.lookup(EQUATION_TOPIC_BASE_NAME + "divide")
        );
    }

    public EquationQueueConsumer equationQueueConsumer() throws JMSException, NamingException {
        final QueueConnection queueConnection = this.queueConnectionFactory.createQueueConnection();
        final QueueSession queueSession = queueConnection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
        final Queue queue = (Queue) this.context.lookup(EQUATION_QUEUE_NAME);
        final QueueReceiver receiver = queueSession.createReceiver(queue);
        final TopicSession topicSession = this.topicConnectionFactory.createTopicConnection()
                .createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
        final Map<String, TopicPublisher> publishers = ImmutableMap.of(
                "+", topicSession.createPublisher((Topic) this.context.lookup(EQUATION_TOPIC_BASE_NAME + "plus")),
                "-", topicSession.createPublisher((Topic) this.context.lookup(EQUATION_TOPIC_BASE_NAME + "minus")),
                "*", topicSession.createPublisher((Topic) this.context.lookup(EQUATION_TOPIC_BASE_NAME + "multiply")),
                "/", topicSession.createPublisher((Topic) this.context.lookup(EQUATION_TOPIC_BASE_NAME + "divide"))
        );
        return new EquationQueueConsumer(receiver, queueConnection, new Solver(), publishers, topicSession,
                Executors.newFixedThreadPool(5));
    }

    public EquationProducer equationProducer() throws JMSException, NamingException {
        final QueueSession queueSession = this.queueConnectionFactory.createQueueConnection()
                .createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
        final Queue queue = (Queue) this.context.lookup(EQUATION_QUEUE_NAME);
        final QueueSender sender = queueSession.createSender(queue);
        return new EquationProducer(sender, queueSession);
    }

    public EquationTopicSubscriber equationTopicSubscriber(final String operation) throws JMSException {
        final Topic topic = this.topics.get(operation);
        final TopicConnection topicConnection = this.topicConnectionFactory.createTopicConnection();
        final TopicSession topicSession = topicConnection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
        final TopicSubscriber subscriber = topicSession.createSubscriber(topic);
        return new EquationTopicSubscriber(topicConnection, subscriber);
    }
}
