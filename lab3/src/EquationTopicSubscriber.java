import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.jms.TopicConnection;
import javax.jms.TopicSubscriber;

public class EquationTopicSubscriber implements Runnable {

    private final TopicConnection topicConnection;
    private final TopicSubscriber topicSubscriber;

    public EquationTopicSubscriber(final TopicConnection topicConnection, final TopicSubscriber topicSubscriber) {
        this.topicConnection = topicConnection;
        this.topicSubscriber = topicSubscriber;
    }

    @Override
    public void run() {
        try {
            this.topicConnection.start();
        }
        catch(final JMSException e) {
            throw new RuntimeException("JMS Error while connecting to topic", e);
        }
        while(!Thread.interrupted()) {
            try {
                final Message message = this.topicSubscriber.receive();
                if(message instanceof TextMessage) {
                    final TextMessage textMessage = (TextMessage) message;
                    System.out.println("Received: " + textMessage.getText());
                }
                else {
                    System.err.println("Unsupported message type: " + message.getJMSType());
                }
            }
            catch(final JMSException e) {
                throw new RuntimeException("JMS Error while receiving message", e);
            }
        }
    }
}
