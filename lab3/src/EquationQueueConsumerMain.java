import javax.jms.JMSException;
import javax.naming.NamingException;

public class EquationQueueConsumerMain {
    public static void main(final String[] args) throws NamingException, JMSException {
        new Configuration().equationQueueConsumer().run();
    }
}
