import java.util.List;
import java.util.Random;

import javax.jms.JMSException;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.TextMessage;

import com.google.common.collect.ImmutableList;

public class EquationProducer implements Runnable {

    private static final List<String> operators = ImmutableList.of("+", "-", "*", "/", "^");

    private final QueueSender sender;
    private final QueueSession queueSession;

    public EquationProducer(final QueueSender sender, final QueueSession queueSession) {
        this.sender = sender;
        this.queueSession = queueSession;
    }

    @Override
    public void run() {
        final Random random = new Random();
        while(!Thread.interrupted()) {
            final String text = random.nextInt(500) + " "
                    + operators.get(random.nextInt(operators.size())) + " "
                    + random.nextInt(500);
            try {
                final TextMessage message = this.queueSession.createTextMessage();
                message.setText(text);
                this.sender.send(message);
            }
            catch(final JMSException e) {
                throw new RuntimeException("JMS Error while publishing", e);
            }
            System.out.println("Successfully published new message: " + text);
/*            try {
                Thread.sleep(10);
            }
            catch(final InterruptedException e) {
                e.printStackTrace();
            }*/
        }
    }
}
