package solver;

import java.util.Map;

import com.google.common.collect.ImmutableMap;

public class Solver {

    private static final Map<String, Operation> operations = ImmutableMap.of(
            "+", (x, y) -> x + y,
            "-", (x, y) -> x - y,
            "*", (x, y) -> x * y,
            "/", (x, y) -> x / y
    );

    public int solve(final String input) throws ParseException {
        final String split[] = input.split(" ");
        if(split.length != 3) {
            throw new ParseException("Invalid input: " + input);
        }
        if(!operations.containsKey(split[1])) {
            throw new ParseException("Unsupported operator: '" + split[1] + "' in: " + input);
        }
        try {
            return operations.get(split[1]).eval(Integer.parseInt(split[0]), Integer.parseInt(split[2]));
        }
        catch(final NumberFormatException e) {
            throw new ParseException("Invalid input:" + input);
        }
    }
}
