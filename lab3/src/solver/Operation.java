package solver;

public interface Operation {
    int eval(int x, int y);
}
