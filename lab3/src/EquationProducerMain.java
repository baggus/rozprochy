import javax.jms.JMSException;
import javax.naming.NamingException;

public class EquationProducerMain {
    public static void main(final String[] args) throws NamingException, JMSException {
        new Configuration().equationProducer().run();
    }
}
