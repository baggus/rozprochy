import javax.jms.JMSException;
import javax.naming.NamingException;

public class EquationTopicSubscriberMain {
    public static void main(final String[] args) throws NamingException, JMSException {
        if(args.length != 1) {
            System.out.println("Please provide desired operation to watch");
            return;
        }
        new Configuration().equationTopicSubscriber(args[0]).run();
    }
}
