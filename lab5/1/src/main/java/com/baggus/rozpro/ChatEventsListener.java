package com.baggus.rozpro;

interface ChatEventsListener {

    void onNewMessage(final String message, final String username, final String channelId);

    void onError(final Exception e);

    void onJoinChannel(final String username, final String channelId);

    void onLeaveChannel(final String username, final String channelId);
}
