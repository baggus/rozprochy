package com.baggus.rozpro;

import static pl.edu.agh.dsrg.sr.chat.protos.ChatOperationProtos.ChatAction.ActionType.JOIN;
import static pl.edu.agh.dsrg.sr.chat.protos.ChatOperationProtos.ChatAction.ActionType.LEAVE;

import pl.edu.agh.dsrg.sr.chat.protos.ChatOperationProtos.ChatAction;
import pl.edu.agh.dsrg.sr.chat.protos.ChatOperationProtos.ChatState;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import org.jgroups.View;

public class Chat {

    private static final String MANAGEMENT_CHANNEL_NAME = "ChatManagement321123";

    private final ChatEventsListener eventsListener;
    private final JChannel managementChannel;
    private final String nickname;

    // channelId -> Channel
    private final Map<String, Channel> channels = new HashMap<>();

    public Chat(final String nickname, final ChatEventsListener eventsListener) throws Exception {
        this.nickname = nickname;
        this.managementChannel = JChannels.newJChannel();
        this.eventsListener = eventsListener;
    }

    public void init() throws Exception {
        this.managementChannel.setReceiver(new Receiver());
        this.managementChannel.setName(this.nickname);
        this.managementChannel.connect(MANAGEMENT_CHANNEL_NAME);
        this.managementChannel.getState(null, 10000);
    }

    public void close() throws Exception {
        for(final Map.Entry<String, Channel> entry : this.channels.entrySet()) {
            final Channel channel = entry.getValue();
            if(channel.isListening()) {
                leaveChannel(entry.getKey());
            }
            channel.close();
        }
        this.channels.clear();
    }

    public void joinChannel(final String channelId) throws Exception {
        final Channel channel;
        if(this.channels.containsKey(channelId)) {
            channel = this.channels.get(channelId);
        }
        else {
            channel = new Channel(channelId, this.nickname, this.eventsListener);
            this.channels.put(channelId, channel);
        }
        channel.join();
        this.managementChannel.send(new Message(null, null, createJoinAction(this.nickname, channelId).toByteArray()));
    }

    public void leaveChannel(final String channelId) throws Exception {
        if(!this.channels.containsKey(channelId)) {
            throw new RuntimeException("There is no such channel as: " + channelId);
        }
        final Channel channel = this.channels.get(channelId);
        channel.leave();
        final ChatAction action = ChatAction.newBuilder()
                .setChannel(channelId)
                .setAction(LEAVE)
                .setNickname(this.nickname)
                .build();
        this.managementChannel.send(new Message(null, null, action.toByteArray()));
    }

    private void onJoinAction(final String channelId, final String userName) throws Exception {
        try {
            Integer.parseInt(channelId);
        }
        catch(NumberFormatException e) {
            return;
        }
        if(this.nickname.equals(userName)) {
            return;
        }
        final Channel channel;
        if(this.channels.containsKey(channelId)) {
            channel = this.channels.get(channelId);
        }
        else {
            channel = new Channel(channelId, this.nickname, this.eventsListener);
            this.channels.put(channelId, channel);
        }
        channel.addUser(userName);
        if(channel.isListening()) {
            this.eventsListener.onJoinChannel(userName, channelId);
        }
    }

    private void onLeaveAction(final String channelId, final String userName) throws Exception {
        if(this.nickname.equals(userName)) {
            return;
        }
        if(this.channels.containsKey(channelId)) {
            final Channel channel = this.channels.get(channelId);
            if(channel.isListening()) {
                this.eventsListener.onLeaveChannel(userName, channelId);
            }
            channel.removeUser(userName);
            if(channel.isEmpty() && !channel.isListening()) {
                channel.close();
                this.channels.remove(channelId);
            }
        }
    }

    public void sendMessage(final String channelId, final String message) throws Exception {
        if(!this.channels.containsKey(channelId)) {
            throw new RuntimeException("There is no such channel as: " + channelId);
        }
        this.channels.get(channelId).sendMessage(message);
    }

    public Collection<String> getUsersInChannel(final String channelId) {
        if(this.channels.containsKey(channelId)) {
            return this.channels.get(channelId).getUsers();
        }
        else {
            return Collections.emptyList();
        }
    }

    public Collection<String> getChannels() {
        return this.channels.keySet();
    }

    public boolean isListening(final String channelId) {
        if(this.channels.containsKey(channelId)) {
            return this.channels.get(channelId).isListening();
        }
        return false;
    }

    private static ChatAction createJoinAction(final String nickname, final String channelId) {
        return ChatAction.newBuilder().setChannel(channelId).setAction(JOIN).setNickname(nickname).build();
    }

    private class Receiver extends ReceiverAdapter {

        @Override
        public synchronized void receive(final Message msg) {
            try {
                final ChatAction chatAction = ChatAction.parseFrom(msg.getBuffer());
                switch(chatAction.getAction()) {
                    case JOIN:
                        onJoinAction(chatAction.getChannel(), chatAction.getNickname());
                        break;
                    case LEAVE:
                        onLeaveAction(chatAction.getChannel(), chatAction.getNickname());
                        break;
                    default:
                        throw new RuntimeException("Unknown action");
                }
            }
            catch(final Exception e) {
                Chat.this.eventsListener.onError(e);
            }
        }

        @Override
        public synchronized void viewAccepted(final View view) {
            final Set<String> activeUsers = view.getMembers()
                    .stream()
                    .map(Object::toString)
                    .collect(Collectors.toSet());
            for(final Channel channel : new HashSet<>(Chat.this.channels.values())) {
                for(final String userInChannel : channel.getUsers()) {
                    if(!activeUsers.contains(userInChannel)) {
                        try {
                            onLeaveAction(channel.getChannelId(), userInChannel);
                        }
                        catch(final Exception e) {
                            Chat.this.eventsListener.onError(e);
                        }
                    }
                }
            }
        }

        @Override
        public synchronized void setState(final InputStream input) throws Exception {
            final ChatState chatState = ChatState.parseFrom(input);
            for(final ChatAction chatAction : chatState.getStateList()) {
                if(chatAction.getAction() == ChatAction.ActionType.JOIN) {
                    onJoinAction(chatAction.getChannel(), chatAction.getNickname());
                }
                else if(chatAction.getAction() == ChatAction.ActionType.LEAVE) {
                    onLeaveAction(chatAction.getChannel(), chatAction.getNickname());
                }
            }
        }

        @Override
        public synchronized void getState(final OutputStream output) throws Exception {
            final ChatState.Builder builder = ChatState.newBuilder();
            for(final Channel channel : Chat.this.channels.values()) {
                for(final String nickname : channel.getUsers()) {
                    builder.addState(createJoinAction(nickname, channel.getChannelId()));
                }
                if(channel.isListening()) {
                    builder.addState(createJoinAction(Chat.this.nickname, channel.getChannelId()));
                }
            }
            output.write(builder.build().toByteArray());
        }
    }
}
