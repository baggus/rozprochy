package com.baggus.rozpro;

import java.util.Scanner;

public class Main {
    public static void main(final String[] args) throws Exception {
        try {
        /*LogManager.getLogManager().reset();
        Logger globalLogger = Logger.getLogger(java.util.logging.Logger.GLOBAL_LOGGER_NAME);
        globalLogger.setLevel(java.util.logging.Level.OFF);*/
            final Scanner scanner = new Scanner(System.in);
            System.out.println("Enter nickname:");
            final String nickname = scanner.nextLine();
            final Chat chat = new Chat(nickname, new Listener());
            chat.init();
            printUsage();
            while(true) {
                final String[] split = scanner.nextLine().split(" ");
                final String action = split[0].toLowerCase();
                switch(action) {
                    case "exit": {
                        chat.close();
                        System.exit(0);
                        break;
                    }
                    case "help": {
                        printUsage();
                        break;
                    }
                    case "list-channels": {
                        chat.getChannels().stream().forEach(System.out::println);
                        break;
                    }
                    case "list-channel": {
                        chat.getUsersInChannel(split[1]).forEach(System.out::println);
                        break;
                    }
                    case "join": {
                        final String channelId = split[1];
                        if(chat.isListening(channelId)) {
                            System.out.println("You have already joined this channel!");
                        }
                        else {
                            chat.joinChannel(split[1]);
                        }
                        break;
                    }
                    case "leave": {
                        final String channelId = split[1];
                        if(!chat.isListening(channelId)) {
                            System.out.println("You must first join this channel!");
                        }
                        else {
                            chat.leaveChannel(split[1]);
                        }
                        break;
                    }
                    case "send": {
                        final String channelId = split[1];
                        if(!chat.isListening(channelId)) {
                            System.out.println("You must first join this channel!");
                        }
                        else {
                            chat.sendMessage(split[1], split[2]);
                        }
                        break;
                    }
                    default:
                        printUsage();
                }
            }
        }
        catch(final Exception e) {
        }
    }

    private static void printUsage() {
        System.out.println("Usage:");
        System.out.println("List-channels");
        System.out.println("List-channel <channel id>");
        System.out.println("Join <channel id>");
        System.out.println("Leave <channel id>");
        System.out.println("Send <channel id> <message>");
        System.out.println("Exit");
        System.out.println("Help");
    }

    private static class Listener implements ChatEventsListener {

        @Override
        public void onNewMessage(final String message, final String username, final String channelId) {
            System.out.println(username + "@Channel-" + channelId + ": " + message);
        }

        @Override
        public void onError(final Exception e) {
            System.out.println("An error occurred " + e);
        }

        @Override
        public void onJoinChannel(final String username, final String channelId) {
            System.out.println(username + " joined Channel-" + channelId);
        }

        @Override
        public void onLeaveChannel(final String username, final String channelId) {
            System.out.println(username + " left Channel-" + channelId);
        }
    }
}
