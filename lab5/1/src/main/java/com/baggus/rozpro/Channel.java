package com.baggus.rozpro;

import pl.edu.agh.dsrg.sr.chat.protos.ChatOperationProtos.ChatMessage;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;

import com.google.protobuf.InvalidProtocolBufferException;

public class Channel {

    private final Set<String> users = new HashSet<>();
    private final String channelId;
    private final JChannel channel;
    private final ChatEventsListener eventsListener;
    private boolean isListening;

    public Channel(final String channelId, final String nickname, final ChatEventsListener listener) throws Exception {
        this.channelId = channelId;
        this.channel = JChannels.newJChannel(channelId);
        this.eventsListener = listener;
        this.channel.setReceiver(new ChannelReceiver());
        this.channel.setName(nickname);
    }

    public void join() throws Exception {
        this.channel.connect(this.channelId);
        this.isListening = true;
    }

    public void leave() {
        this.channel.disconnect();
        this.isListening = false;
    }

    public void close() {
        this.channel.close();
    }

    public boolean isEmpty() {
        return this.users.isEmpty();
    }

    public void addUser(final String nickname) {
        this.users.add(nickname);
    }

    public void removeUser(final String username) {
        this.users.remove(username);
    }

    public void sendMessage(final String msg) throws Exception {
        final ChatMessage message = ChatMessage.newBuilder()
                .setMessage(msg)
                .build();
        final Message jMessage = new Message(null, null, message.toByteArray());
        this.channel.send(jMessage);
    }

    public boolean isListening() {
        return this.isListening;
    }

    public Collection<String> getUsers() {
        return new HashSet<>(this.users);
    }

    public String getChannelId() {
        return this.channelId;
    }

    private class ChannelReceiver extends ReceiverAdapter {
        @Override
        public void receive(final Message msg) {
            try {
                final ChatMessage message = ChatMessage.parseFrom(msg.getBuffer());
                Channel.this.eventsListener.onNewMessage(
                        message.getMessage(),
                        msg.getSrc().toString(),
                        Channel.this.channelId);
            }
            catch(final InvalidProtocolBufferException e) {
                Channel.this.eventsListener.onError(e);
            }
        }
    }
}
